# W3_Band

HTML/CSS basic with F8 course

Bạn sẽ học được gì?

- Biết cách xây dựng giao diện web với HTML, CSS
- Biết cách đặt tên class CSS theo chuẩn BEM
- Làm chủ Flexbox khi dựng bố cục website
- Biết cách tự tạo động lực cho bản thân
- Biết cách phân tích giao diện website
- Biết cách làm giao diện web responsive
- Sở hữu 2 giao diện web khi học xong khóa học
- Biết cách tự học những kiến thức mới
